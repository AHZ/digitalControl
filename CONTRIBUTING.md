# Getting Started with Git
It is best to follow one of the simple guides in the internet like this one [here](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html)

# Git and Branch
In order to work on an issue or on a chapter, you need to create a branch, switch to that branch, do your work, commit and push your changes. Once all the work on the chapter is done the branch may be deleted. Once the branch work is finalized it may be may be merged with the main branch called master.

To create a new branch and switch to it
+ `git checkout -b introduction`

The above line creates the "introduction" branch and switches to it. If a branch already exists in locall and  you want to sync and switch to it:
+ `git checkout introduction`
+ `git pull origin introduction`

if the branch is not in locall but it exist in the git server and  you want to sync and switch to it:
+ `git checkout -b origin/introduction introduction`
+ `git pull origin introduction`

Then you can do your work and finally:
+ `git commit -a -m "your message"`
+ `git push origin introduction`

Note that if in between your work you wished to change your branch it is possible with the `git checkout nameOfBranch` command. 
If you decide that the work on a branch is completely finished you can merge all the changes to the master branch using requesting merge request from the https://gitlab.com/AmirJ/bp-book
As you are not permmited to push the master branch.it is protected!:)

Note that you should already be in the introduction branch otherwise it merges master with introduction. `checkout` to master first.
This command merges all the changes that have been made in the introduction branch into the master branch.

After Your work is done you can delete the branch:
Branches can be deleted locally and remotely using: 
+ `git branch -d introduction`
+ `git push origin --delete introduction`

# Requirements
+ Windows: [GitBash](https://git-for-windows.github.io/)
+ Linux: Use your package repository

